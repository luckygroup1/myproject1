#provider connecting
terraform {
  required_providers {
    yandex = {
      source                    = "yandex-cloud/yandex"
      version                   = "0.89.0"
    }
  }
backend "http" {
  }
}

#variables
variable "resources_vm" {}
variable "resources_net" {}
variable "zone" {}
variable "cloud_id" {}
variable "folder_id" {}
variable "name_subdomains" {}
variable "id_vm_for_domain_apps1" {}
variable "provider_token" {
  sensitive = true
}
variable "ssh_key" {}


#provider declaration
provider "yandex" {
  token                         = var.provider_token
  cloud_id                      = var.cloud_id
  folder_id                     = var.folder_id
  zone                          = var.zone

}

#creation of vm

data "yandex_compute_image" "ubuntu_image" {
  count                         = length(var.resources_vm)
  family                        = var.resources_vm[count.index].image

}

data "yandex_dns_zone" "zone" {

  name                          = var.resources_net.dns_zone
}

resource "yandex_vpc_network" "network_terraform" {
  name                          = "net_terraform"
}

resource "yandex_vpc_subnet" "subnet_terraform" {
  name                          = var.resources_net.subnet_name
  zone                          = var.zone
  network_id                    = yandex_vpc_network.network_terraform.id
  v4_cidr_blocks                = var.resources_net.subnet_net
}

resource "yandex_compute_instance" "test1"{
  count                         = length(var.resources_vm)
  name                          = var.resources_vm[count.index].instance_name

  resources {
    cores                       = var.resources_vm[count.index].cores
    memory                      = var.resources_vm[count.index].memory
  }

  boot_disk {
    initialize_params {
      image_id                  = data.yandex_compute_image.ubuntu_image[count.index].id
    }
  }

  network_interface {
    subnet_id                   = yandex_vpc_subnet.subnet_terraform.id
    nat                         = true
    
  }

  metadata = {
    ssh-keys = "ubuntu:${var.ssh_key}"
  }

}

resource "yandex_dns_recordset" "subdomains" {
  count                         = length(var.resources_vm)
  name                          = var.resources_vm[count.index].instance_name
  zone_id                       = data.yandex_dns_zone.zone.id
  type                          = var.resources_net.dns_type
  ttl                           = var.resources_net.dns_ttl
  data                          = ["${yandex_compute_instance.test1[count.index].network_interface.0.nat_ip_address}"]  
}

resource "yandex_dns_recordset" "subdomains_for_apps_VM1" {
  count                         = length(var.name_subdomains.list_apps_1)
  name                          = var.name_subdomains.list_apps_1[count.index]
  zone_id                       = data.yandex_dns_zone.zone.id
  type                          = var.resources_net.dns_type
  ttl                           = var.resources_net.dns_ttl
  data                          = ["${yandex_compute_instance.test1[var.id_vm_for_domain_apps1].network_interface.0.nat_ip_address}"]
}

data "template_file" "data_json" {
  template                      = "${file("./data.json.tmpl")}"
  count                         = "${length(var.resources_vm)}"

  vars  =  {   
    host                        = "${yandex_compute_instance.test1[count.index].name}"
    address                     = "${yandex_compute_instance.test1[count.index].network_interface.0.nat_ip_address}"
  }
 }


data "template_file" "service_json" {
  template                      = "${file("./service.json.tmpl")}"
  vars  =  {
     value                      = "${join("\n", data.template_file.data_json.*.rendered)}"
   }
  }

resource "local_file" "ansible_inventory" {
  count                         = "${length(var.resources_vm)}"

  content                       = "${data.template_file.service_json.rendered}"
  filename                      = "./hosts.yml"
}